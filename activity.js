console.log("Hello Thursday")

let totalNumbersOfFruitsOnSale = `[
	{
    	"_id": "ObjectId("641c49f3dea6eeecfed165e2")",
       	"totalNumbersOfFruitsOnSale": 3
	}
]`;

console.log(totalNumbersOfFruitsOnSale);

/*

db.fruits.aggregate([
	    {$match: {onSale: true}},
	    {$count: "totalNumbersOfFruitsOnSale"},
	    {$out: "totalNumbersOfFruitsOnSale"}
]); 

*/

let enoughStock = `[
	{
		"_id": "ObjectId("641c4b4fdea6eeecfed23e9c")",
		"enoughStock": 2
	}
]`;

console.log(enoughStock);
/*

db.fruits.aggregate([
	    {$match: {stock: {$gte: 20}}},
	    {$count: "enoughStock"},
	    {$out: "enoughStock"}
]);

*/


let avg_price = `[
	{
		"id": 1.0,
		"avg_price": 45.0
	},
	{
		"id": 2.0,
		"avg_price": 20.0
	}
]`;

console.log(avg_price);

/*

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id:"$supplier_id", avg_price: {$avg: "$price"}}},
    {$out: "avg_price"}
]);

*/

let max_price = `[
	{
		"id": 2.0,
		"max_price": 20.0
	},
	{
		"id": 1.0,
		"max_price": 50.0
	}
]`;

console.log(max_price);

/*

db.fruits.aggregate([
    {$group: {_id:"$supplier_id", max_price: {$max: "$price"}}},
    {$out: "max_price"}
]); 

*/


let min_price = `[
	{
		"id": 2.0,
		"min_price": 20.0
	},
	{
		"id": 1.0,
		"min_price": 40.0
	}
]`;

console.log(min_price);


/*


	db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id:"$supplier_id", min_price: {$min: "$price"}}},
    {$out: "min_price"}
	]);    
	

*/